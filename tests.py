from combinaisons import get_combinaisons
import random

# Generate a list of subNumbers of len between minNumbers and maxNumbers. The maximum value of numbers inside the list is maxValue.
def generateSubNumbers(minNumbers, maxNumbers, maxValue=1000):
    res = []
    number = random.randint(minNumbers, maxNumbers)
    for i in range(number):
        res.append(round(random.uniform(0, maxValue), 1))
    res.sort()
    return res

# Generate a number based on a subNumber list, using a maximum of maxSubNumbersUsed to compose the number
def generateNumber(subNumbers, maxSubNumbersUsed=5):
    number = 0
    for i in range(maxSubNumbersUsed):
        number += subNumbers[random.randrange(0, len(subNumbers))]
    return number

# Perform nbTests consisting in generating subNumbers, then create a number based on those and try to retrace what were the combinaisons used to generate the number
def testCombinations(nbTests):
    success = True
    for i in range(nbTests):
        subNumbers = generateSubNumbers(4,4)
        number = generateNumber(subNumbers)
        combinaisions = get_combinaisons(number, subNumbers)
        print(len(combinaisions))
        success = success and len(combinaisions) != 0
    return success