# genshin-artifact-api

Simple Flask Api used as backend for the project [genshin-artifact](https://gitlab.com/Depfrost/genshin-artifact).
This API is hosted [here](https://genshin-artifact-api-prod.herokuapp.com/) on Heroku.