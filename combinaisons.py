def get_combinaison_helper(finalNumber, subNumbers, maxIndex):
    if (maxIndex < 0):
        return []
    
    res = []
    counter = 0
    number = finalNumber
    while (number > 0):
        tmpRes = get_combinaison_helper(number, subNumbers, maxIndex-1)
        for elem in tmpRes:
            elem[maxIndex] += counter
        res.extend(tmpRes)
        number = round(number - subNumbers[maxIndex], 1)
        counter += 1
    if (number == 0):
        tmpRes = [0] * len(subNumbers)
        tmpRes[maxIndex] = counter
        res.append(tmpRes)
    return res

# find the combinaisions of subNumbers used to generate the finalNumber. (subNumbers can have up to 1 decimal)
def get_combinaisons(finalNumber, subNumbers):
    maxIndex = len(subNumbers) - 1
    res = get_combinaison_helper(finalNumber, subNumbers, maxIndex)
    return res