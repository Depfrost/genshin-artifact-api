from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort
from artifacts import getArtifactTiers, getStatTiers, processArtifact, artifactSchema, artifactStats, artifactValidStars
from flask_cors import CORS
import json
import jsonschema

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config['CORS_HEADERS'] = 'Content-Type'

def abort_if_invalid_stars(stars):
    valid = False
    for value in artifactValidStars:
        if (value == stars):
            valid = True
            break
    if (not valid):
        abort(400, message="{} is an invalid number of stars.".format(stars))

def abort_if_invalid_stat(stat):
    valid = False
    for value in artifactStats:
        if (value == stat):
            valid = True
            break
    if (not valid):
        abort(400, message="{} is an invalid stat.".format(stat))

def validateJSON(jsonData):
    try:
        res = json.loads(jsonData)
        return res
    except ValueError as err:
        abort(400, message="Json is not valid. {}".format(err))

def validateSchemaJSON(jsonData, jsonSchema):
    try:
        jsonschema.validate(instance=jsonData, schema=jsonSchema)
    except jsonschema.exceptions.ValidationError as err:
        abort(400, message="Json does not respect the required format. {}".format(err))

class ProcessArtifact(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('artifact', type=str, required=True, help="artifact parameter is required!")
        args = parser.parse_args()
        artifact = validateJSON(args['artifact'])
        validateSchemaJSON(artifact, artifactSchema)
        artifact = processArtifact(artifact)
        return artifact

class Artifact(Resource):
    def get(self, artifact_stars=5):
        stats_args = reqparse.RequestParser()
        for stat in artifactStats:
            stats_args.add_argument(stat, type=float, help="{} of your artifact".format(stat))
        abort_if_invalid_stars(artifact_stars)
        args = stats_args.parse_args()
        res = {}
        for arg in args:
            if (args[arg] is not None):
                res[arg] = getArtifactTiers(artifact_stars, arg, args[arg])
        return res

class Tiers(Resource):
    def get(self,artifact_stars=5, stat="default"):
        abort_if_invalid_stars(artifact_stars)
        res = {}
        if (stat == "default"):
            for stat in artifactStats:
                res[stat] = getStatTiers(artifact_stars, stat)
        else:
            abort_if_invalid_stat(stat)
            res[stat] = getStatTiers(artifact_stars, stat)
        return res

api.add_resource(ProcessArtifact, '/')
api.add_resource(Tiers, '/tiers/', '/tiers/<int:artifact_stars>/', '/tiers/<int:artifact_stars>/<string:stat>')
api.add_resource(Artifact, '/artifact/', '/artifact/<int:artifact_stars>')

if __name__ == "__main__":
    app.run(debug=True)