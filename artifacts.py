from combinaisons import get_combinaisons

artifactStats = ["def_flat", "hp_flat", "atk_flat", "elemental_mastery", "def_percent", "hp_percent", "atk_percent", "energy_recharge", "crit_rate", "crit_damage"]
statsNamePattern = "^(" + "|".join(artifactStats) + ")$"
artifactValidStars = [4, 5]
artifactBaseNbRolls = {
    "4": {
        "min": 2,
        "max": 3
    },
    "5": {
        "min": 3,
        "max": 4
    }
}

# Tiers
possibleTiers = ["C", "B", "A", "S"]

# 5 star artifacts stats tiers
def_flat5 = [16, 19, 21, 23]
hp_flat5 = [209, 239, 269, 299]
atk_flat5 = [14, 16, 18, 19]
elemental_mastery5 = [16, 19, 21, 23]
def_percent5 = [5.1, 5.8, 6.6, 7.3]
hp_percent5 = [4.1, 4.7, 5.3, 5.8]
atk_percent5 = [4.1, 4.7, 5.3, 5.8]
energy_recharge5 = [4.5, 5.2, 5.8, 6.5]
crit_rate5 = [2.7, 3.1, 3.5, 3.9]
crit_damage5 = [5.4, 6.2, 7.0, 7.8]

# 4 star artifacts stats tiers
def_flat4 = [13, 15, 17, 19]
hp_flat4 = [167, 191, 215, 239]
atk_flat4 = [11, 12, 14, 16]
elemental_mastery4 = [13, 15, 17, 19]
def_percent4 = [4.1, 4.7, 5.3, 5.8]
hp_percent4 = [3.3, 3.7, 4.2, 4.7]
atk_percent4 = [3.3, 3.7, 4.2, 4.7]
energy_recharge4 = [3.6, 4.1, 4.7, 5.2]
crit_rate4 = [2.2, 2.5, 2.8, 3.1]
crit_damage4 = [4.4, 5.0, 5.6, 6.2]

# Schema of an artifact
artifactSchema = {
    "type": "object",
    "properties": {
        "stars": {"type": "number", "minimum": 4, "maximum": 5},
        "level": {"type": "number", "minimum": 0, "maximum": 20},
        "valid": {"type": "boolean"},
        "remainingRolls": {"type": "number", "minimum": 0},
        "tier": {"type": "string"},
        "stats": {
            "type": "array",
            "maxItems": 4,
            "items": {
                "type": "object",
                "properties": {
                    "name": {"type": "string", "pattern": statsNamePattern},
                    "value": {"type": "number", "minimum": 0},
                    "useful": {"type": "boolean"},
                    "valid": {"type": "boolean"},
                    "tier": {"type": "string"},
                    "rolls": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "tier": {"type": "string"},
                                "value": {"type": "number"}
                            },
                            "required": ["tier", "value"]
                        }
                    }
                },
                "required": ["name", "value", "useful", "valid", "tier", "rolls"]
            }
        }
    },
    "required": ["stars", "level", "valid", "remainingRolls", "tier", "stats"]
}

# Return the value of a tier
def switchTier(arg):
    switcher = {
        "S": 4,
        "A": 3,
        "B": 2,
        "C": 1
    }
    return switcher.get(arg, 0)

def getArtifactTiers(stars, stat, value):
    statTiers = stat + str(stars)
    combinaisons = get_combinaisons(value, eval(statTiers))
    res = []
    for combinaison in combinaisons:
        item = {}
        for i in range(len(combinaison)):
            if (combinaison[i] != 0):
                item["tier"+str(i+1)] = combinaison[i]
        res.append(item)
    return res

def getStatTiers(stars, stat):
    statTiers = eval(stat + str(stars))
    res = {}
    for i in range(len(statTiers)):
        res["tier"+str(i+1)] = statTiers[i]
    return res

# Return the tier of a stat given its rolls
def getStatTier(stat):
    points = 0
    maxPoints = 0
    for roll in stat["rolls"]:
        points += switchTier(roll["tier"])
        maxPoints += switchTier("S")
    index = int(round((points/maxPoints)*len(possibleTiers))) - 1
    return possibleTiers[index]

# Return the tier of a stat roll
def getStatRollTier(stars, statName, value):
    statPossibleValues = eval(statName + str(stars))
    for i in range(len(statPossibleValues)):
        if (statPossibleValues[i] == value):
            return possibleTiers[i]
    return ""

# Return the rolls of a stat
def getStatRolls(stars, statName, value):
    res = []
    statPossibleValues = eval(statName + str(stars))
    combinaisons = get_combinaisons(value, statPossibleValues)
    if combinaisons:
        combinaison = combinaisons[0]
        for i in range(len(combinaison)):
            for x in range(combinaison[i]):
                roll = {}
                roll["value"] = statPossibleValues[i]
                roll["tier"] = getStatRollTier(stars, statName, statPossibleValues[i])
                res.append(roll)
    return res

# Process a stat to get its rolls and tier
def processArtifactStat(stars, stat):
    stat["rolls"] = getStatRolls(stars, stat["name"], stat["value"])
    if (stat["rolls"] != []):
        stat["valid"] = True
        stat["tier"] = getStatTier(stat)
    else:
        stat["valid"] = False
        stat["tier"] = ""
    return stat

# Process all artifact stats to get their rolls and tiers
def processArtifactStats(stars, stats):
    for stat in stats:
        stat = processArtifactStat(stars, stat)
    return stats

# Get the minimum/maximum number of rolls for a level 0 artifact (minmax can be either "min" or "max")
def getArtifactBaseNbRolls(stars, minmax):
    return artifactBaseNbRolls[str(stars)][minmax]

# Get artifact number of rolls
def getArtifactNbRolls(stats):
    nbRolls = 0
    for stat in stats:
        nbRolls += len(stat["rolls"])
    return nbRolls

# Check if an artifact is valid (considering stats have already been validated. Does not check duplicated stats. Does not test number of different stats.)
def isArtifactValid(artifact):
    nbRolls = getArtifactNbRolls(artifact["stats"])
    for stat in artifact["stats"]:
        if (not stat["valid"]):
            return False
    nbRollsForLevel = artifact["level"] // 4
    minNbRolls = getArtifactBaseNbRolls(artifact["stars"], "min") + nbRollsForLevel
    maxNbRolls = getArtifactBaseNbRolls(artifact["stars"], "max") + nbRollsForLevel
    if (nbRolls < minNbRolls or nbRolls > maxNbRolls):
        return False
    return True

# Get the number of remaining rolls for an artifact
def getArtifactRemainingRolls(level):
    return ((20 - level) // 4)

# Compute the tier of an artifact based on its rolls
def getArtifactTier(artifact):
    # pity is a number between 0 and 1 allowing for better tier the higher it is (a score of 1-(pity*nbRolls) is needed for S tier)
    pity = 0
    # statTierFactor is the weight of stat tiers in computing the artifact tier
    statTierWeight = 0.25
    score = 0
    maxScore = 0
    nbRolls = 0
    for stat in artifact["stats"]:
        usefulFactor = 1 if stat["useful"] == True else 0
        for roll in stat["rolls"]:
            score += usefulFactor * (statTierWeight * switchTier(roll["tier"]))
            maxScore += 1 * (statTierWeight * switchTier("S"))
            nbRolls += 1
    # No rolls at all on artifact (invalid)
    if (nbRolls == 0):
        return ""
    # Allow for more pity as more rolls are made
    pity *= nbRolls
    index = int(round(min((score/maxScore) + pity, 1)*(len(possibleTiers) - 1)))
    return possibleTiers[index]

# Process an artifact (validity, rolls, tiers)
def processArtifact(artifact):
    artifact["stats"] = processArtifactStats(artifact["stars"], artifact["stats"])
    # comppute artifact (valid, remainingRolls, tier)
    artifact["valid"] = isArtifactValid(artifact)
    artifact["remainingRolls"] = getArtifactRemainingRolls(artifact["level"])
    artifact["tier"] = getArtifactTier(artifact)
    return artifact